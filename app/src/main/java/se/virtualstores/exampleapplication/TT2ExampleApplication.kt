package se.virtualstores.exampleapplication

import android.app.Application
import android.util.Log
import se.virtualstores.tt2.androidsdk.TT2SDK

class TT2ExampleApplication : Application() {
    companion object {
        private const val TAG = "TT2ExampleApplication"
    }

    override fun onCreate() {
        super.onCreate()
    }

}