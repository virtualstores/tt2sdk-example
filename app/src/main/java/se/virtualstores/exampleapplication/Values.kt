package se.virtualstores.exampleapplication

object Values {
    internal const val CLIENT_ID = 1L
    internal const val STORE_ID = 1L
    internal const val CENTRAL_API_KEY = BuildConfig.CENTRAL_SERVER_APIKEY
    internal const val CENTRAL_API_URL = BuildConfig.CENTRAL_SERVER_URL
}