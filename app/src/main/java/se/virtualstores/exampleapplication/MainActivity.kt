package se.virtualstores.exampleapplication

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.main_activity.*
import se.virtualstores.exampleapplication.main.MapFragment
import se.virtualstores.exampleapplication.main.ScannerFragment
import se.virtualstores.tt2.androidsdk.TT2SDK
import se.virtualstores.tt2.androidsdk.map.model.MapOptions
import se.virtualstores.tt2.androidsdk.scanner.HandlerScannerFragment
import kotlin.random.Random
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import se.virtualstores.tt2.androidsdk.map.ScanLocation
import se.virtualstores.tt2.androidsdk.model.Item
import se.virtualstores.tt2.androidsdk.model.Message
import se.virtualstores.tt2.androidsdk.util.ItemPosition
import kotlin.math.atan2

class MainActivity
    : AppCompatActivity(),
    HandlerScannerFragment.Handler,
    TT2SDK.MessageListener {

    companion object {
        private const val TAG = "MainActivity"

        internal const val MAP_FRAGMENT = "mapFragment"
        internal const val SCANNER_FRAGMENT = "scannerFragment"
    }

    private lateinit var start: ScanLocation
    private val mapFragment: MapFragment?
        get() = supportFragmentManager.findFragmentByTag(MAP_FRAGMENT) as? MapFragment

    private val scannerFragment: ScannerFragment?
        get() = supportFragmentManager.findFragmentByTag(SCANNER_FRAGMENT) as? ScannerFragment

    private val mapOptions = MapOptions().apply {
        cameraLocked = false
        pathStyleHead = MapOptions.LineStyle(
            lineColor = "#FF9100",
            lineWidth = 8.0f,
            lineOpacity = 0.8f,
            lineJoin = MapOptions.LineStyle.LINE_JOIN_ROUND,
            lineCap = MapOptions.LineStyle.LINE_CAP_ROUND
        )
        pathStyleTail = MapOptions.LineStyle(
            lineWidth = 5.0f,
            lineColor = "#FF9100",
            lineOpacity = 0.25f,
            lineJoin = MapOptions.LineStyle.LINE_JOIN_ROUND,
            lineCap = MapOptions.LineStyle.LINE_CAP_ROUND
        )
        activeAccuracyStyle = MapOptions.CircleStyle(
            color = Color.parseColor("#eb9f60"),
            alpha = 0.5f
        )
        staleAccuracyStyle = MapOptions.CircleStyle(
            color = Color.parseColor("#eb9f60"),
            alpha = 0.5f
        )
        clusterMaxZoom = 6
        clusterRadius = 35
        userMarkTint = Color.parseColor("#eb9f60")
        mapboxLogoGravity = Gravity.BOTTOM or Gravity.START
        startImage = R.drawable.ic_qr
        goalImage = R.drawable.ic_flag
        showPathfindingTail = true
        markerFallbackImageResource = R.drawable.ic_close
//        debugControlsEnabled = BuildConfig.DEBUG
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        initializeTT2()
        TT2SDK.storesLiveData.observe(this) { stores ->
            Log.d("test", "stores: ${stores.size}")
        }
    }

    private fun initializeMap() {
        val fragment = TT2SDK.getMapData()?.bounds?.let { bounds ->
            MapFragment.newInstance(
                bounds = bounds,
                options = mapOptions
            )
        }

        fragment?.let { mapFragment ->
            supportFragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.mapContainer, mapFragment, MAP_FRAGMENT)
                .commit()
        }
        start = TT2SDK.stores.first { x -> x.id == Values.STORE_ID }.startScanLocations.first()
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        (fragment as? ScannerFragment)?.handler = this
    }

    fun onViewClicked(view: View) {
        when (view) {
            scannerButton -> {
                openScanner()
            }
            quickAddMarkerFAB -> {
                mapFragment?.addMapMark(getRandomItem()) {
                    println("quickAddedMark")
                }
            }
            quickRemoveMarkerFAB -> {
                mapFragment?.removeRandomMark()
            }
            pathfindingButton -> {
                mapFragment?.togglePathfinding()
            }
            startNavigationFab -> {
                mapFragment?.startNavigation(
                    PointF(start.position.x, start.position.y),
                    start.direction
                )
            }
            finalizeFab -> {
                TT2SDK.finalizeSession(applicationContext)
            }
            else -> Unit
        }
    }

    override fun onScanCompleted(result: String?, bitmap: Bitmap?, offset: Double?) {
        when {
            result?.contains("start")!! -> {
                mapFragment?.startNavigation(
                    PointF(start.position.x, start.position.y),
                    start.direction
                )
            }
            result.contains("addMark") -> {
                mapFragment?.addMapMark(getRandomItem()) {
                    println("Mark added by scanner")
                }
            }
            result.contains("find") -> {
                mapFragment?.showPathfinding()
            }
            else -> {
                TT2SDK.getItemPosition(result) { positions, item ->
                    item?.let {
                        TT2SDK.syncPosition(it)
                    } ?: Log.i(TAG, "No such item")
                }
            }
        }

        closeScanner()
    }

    // Indicates that the user has canceled out of the attached ScannerFragment
    override fun onCancel() = closeScanner()

    override fun onMessagePresented(message: Message) {
        Snackbar.make(
            findViewById(R.id.root),
            "${message.title} - ${message.description}",
            Snackbar.LENGTH_LONG
        ).show()
    }

    private fun getRandomItem(): Item {
        val id = Random.nextInt().toString()
        val point = randomPointInBounds()
        println("point: $point")
        return Item("TestMark", id, ItemPosition(1L, point.x, point.y, 0.0f, 0.0f))
    }

    private fun randomPointInBounds(): PointF = TT2SDK.getMapData()?.bounds?.let {
        val offset = 6f
        Random.nextPointF(it.width()/50.0f - offset, it.height()/50.0f - offset).apply {
            x += offset / 2
            y += offset / 2
        }
    } ?: PointF(0f, 0f)

    private fun openScanner() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.scannerContainer, ScannerFragment(), SCANNER_FRAGMENT)
            .commit()
    }

    private fun closeScanner() {
        scannerFragment?.let {
            supportFragmentManager.beginTransaction()
                .remove(it)
                .commit()
        }
    }

    private fun initializeTT2() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
            broadcastReceiver,
            IntentFilter(TT2SDK.POSITIONING_READY)
        )

        TT2SDK.initialize(
            applicationContext,
            Values.CENTRAL_API_URL,
            Values.CENTRAL_API_KEY,
            Values.CLIENT_ID,
            initFinished = {
                Log.d(TAG, "Init finished")
                TT2SDK.addMessageListener(this)
                TT2SDK.initStore(applicationContext, Values.STORE_ID) {
                    initializeMap()
                }
            }
        )
    }

    private fun Random.nextPointF(width: Float, height: Float): PointF =
        PointF(
            this.nextInt(0, width.toInt()).toFloat(),
            this.nextInt(0, height.toInt()).toFloat()
        )

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d(TAG, "Positioning is ready for start code scan")
        }
    }
}