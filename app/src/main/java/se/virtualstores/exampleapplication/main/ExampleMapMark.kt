package se.virtualstores.exampleapplication.main

import com.mapbox.geojson.Point
import se.virtualstores.exampleapplication.R
import se.virtualstores.tt2.androidsdk.map.model.MapMark
import se.virtualstores.tt2.androidsdk.map.model.MapPopup
import se.virtualstores.tt2.androidsdk.map.model.TextImageMapPopup

class ExampleMapMark(
    id: String,
    position: Point,
    triggerRadius: Double,
    imageUrl: String? = null,
    decoration: Decoration? = null
) : MapMark(
    id,
    position,
//    quantity = id.toFloat(),
    triggerRadius = triggerRadius,
    imageURL = imageUrl,
    decoration = decoration
)