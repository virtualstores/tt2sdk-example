package se.virtualstores.exampleapplication.main

import android.graphics.PointF
import android.graphics.RectF
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.lifecycle.observe
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import se.virtualstores.exampleapplication.R
import se.virtualstores.tt2.androidsdk.TT2SDK
import se.virtualstores.tt2.androidsdk.map.BaseMapFragment
import se.virtualstores.tt2.androidsdk.map.IMapFragment
import se.virtualstores.tt2.androidsdk.map.model.MapMark
import se.virtualstores.tt2.androidsdk.map.model.MapOptions
import se.virtualstores.tt2.androidsdk.map.view.MapView
import se.virtualstores.tt2.androidsdk.model.Item
import se.virtualstores.tt2.androidsdk.model.PathfindingGoal
import se.virtualstores.tt2.androidsdk.model.Trigger
import se.virtualstores.tt2.androidsdk.positionkit.IPositionKit
import se.virtualstores.tt2.androidsdk.util.*
import se.virtualstores.tt2.androidsdk.util.navigation.DefaultMapFragmentAdapter
import se.virtualstores.tt2.androidsdk.util.navigation.converter.PixelCoordinateConverter
import kotlin.random.Random

class MapFragment : BaseMapFragment(),
    IMapFragment.Listener,
    IMapFragment.LifecycleListener,
    TT2SDK.TriggerListener {

    companion object {
        private const val TAG = "MapFragment"

        private const val KEY_SESSION_PRE_ACTIVATED = "MapFragmentSessionPreActivated"
        private const val KEY_MAP_SIZE_PIXELS = "MapFragmentMapPixelSize"

        private const val MARKER_IMAGE =
            "https://purepng.com/public/uploads/medium/purepng.com-red-appleapplegreenhealthycut-641522015209cgwcq.png"

        fun newInstance(
            bounds: RectF,
            options: MapOptions?,
            sessionActivated: Boolean = false
        ) = MapFragment().apply {
            val converter = PixelCoordinateConverter(bounds.height(), 50f, 1000f)
            arguments = Bundle().apply {
                putParcelable(KEY_MAP_SIZE_PIXELS, bounds)
                putParcelable(IMapFragment.KEY_BOUNDS, bounds.toLatLngBounds(converter))
                putSerializable(IMapFragment.KEY_OPTIONS, options)
                putSerializable(IMapFragment.KEY_CONVERTER, converter)
                putBoolean(KEY_SESSION_PRE_ACTIVATED, sessionActivated)
            }
        }
    }

    override fun onMapLoadingFailed(numFailedTries: Int): Boolean {
        if (numFailedTries >= 5) {
            Log.e(TAG, "Failed this much $numFailedTries")
            return true
        }
        return false
    }

    override val mapView: MapView?
        get() = view?.findViewById(R.id.mapView)
    override val resetMapButton: ImageButton?
        get() = view?.findViewById(R.id.resetMapButton)
    override val positionButton: ImageButton?
        get() = view?.findViewById(R.id.positionButton)
    override var lifecycleListener: IMapFragment.LifecycleListener? = null

    private lateinit var mapSize: RectF
    private var snackbar: Snackbar? = null
    private var triggeredMark: MapMark? = null

    override val decorations = mapOf(
        "cart" to R.drawable.ic_deco_cart,
        "bonus" to R.drawable.ic_deco_bonus,
        "eco" to R.drawable.ic_deco_eco,
        "flower" to R.drawable.ic_deco_flower
    )

    override fun parseArguments(bundle: Bundle) {
        super.parseArguments(bundle)
        this.mapSize = bundle.getParcelable(KEY_MAP_SIZE_PIXELS)
            ?: throw IllegalArgumentException("Missing mapSize")
    }

    override fun positioningSystemReady() {
        println("Positioning system is ready and can now update your position")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val root = inflater.inflate(R.layout.fragment_map, container, false)
        lifecycleListener = this

        return root
    }

    override fun onClusterClicked(marks: List<MapMark>) {
        Log.d(TAG, "${marks.size} marks clicked")
    }

    override fun onMarkClicked(mark: MapMark) {
        Log.d(TAG, "mark with id ${mark.id} clicked")
        markList.remove(mark)
        pathfindingController.removeGoal(mark.id)
        removeMark(mark.id)
    }

    override fun onStyleLoaded(style: Style, map: MapboxMap) {
        super.onStyleLoaded(style, map)

        listener = this
        adapter = DefaultMapFragmentAdapter()

        setDegreesFromNorth(180.0)

        pathfindingController.startPathfinding(5f) {
            Log.d(TAG, "trigger pathfinding goal radius")
        }

        markerController.addMarkerTriggerListener { mark ->
            if (snackbar == null &&
                mark != triggeredMark &&
                mark is ExampleMapMark
            ) {
                triggeredMark = mark
                snackbar = displayTriggerSnackbar(mark)
            }
        }

        TT2SDK.addTriggerListener(this)
        markerController.setStartVisible(true)
        markerController.setStopVisible(false)

        pathfindingController.sortedGoalsLiveData.observe(viewLifecycleOwner) {
            val str = it?.joinToString(separator = ":") { goal -> goal.id }
        }
    }

    var newId: Int = 0
    private val markList = ArrayList<MapMark>()
    fun addMapMark(item: Item, callback: () -> Unit) {
        item.itemPosition?.toLatLng()?.let { itemPosition ->
            println("ItemPosition: $itemPosition")
            val mark = ExampleMapMark(
                newId++.toString(),
                itemPosition.toPoint(),
                decoration = getDecoration(setOf("cart", "bonus", "eco", "flower").random()),
                triggerRadius = converter.convertToMapMeters(5.0),
                imageUrl = if (Random.nextBoolean()) MARKER_IMAGE else if (Random.nextBoolean()) "" else "hej"
            )
            markList.add(mark)
            this.addMark(mark)
            pathfindingController.addGoal(PathfindingGoal(mark.id, itemPosition, item), callback)
        }
    }

    fun addMapMarks(points: List<PointF>, callback: () -> Unit) {
        val marks = points.map {
            ExampleMapMark(
                newId++.toString(),
                it.toMapboxPoint(converter),
                decoration = getDecoration(setOf("cart", "bonus", "eco", "flower").random()),
                triggerRadius = converter.convertToMapMeters(5.0),
                imageUrl = ""
            )
        }
        markList.addAll(marks)
        marks.forEach(::addMark)
        pathfindingController.addGoals(marks.map {
            PathfindingGoal(
                it.id,
                it.position.latLng(),
                it
            )
        }, callback)
    }

    fun removeRandomMark() {
        if(markList.isEmpty()) return
        val mark = markList.random()
        pathfindingController.removeGoal(mark.id)
        removeMark(mark)
        markList.remove(mark)
    }

    fun showPathfinding() {
        pathfindingController.showPathfinding()
        markerController.setStartVisible(false)
        markerController.setStopVisible(true)
    }

    override fun onHandlingChanged(handling: IPositionKit.Handling) {
        super.onHandlingChanged(handling)
        println("Handling: $handling")
    }

    override fun onTriggerActivated(trigger: Trigger) {
        displayTriggerSnackbar(trigger.id)
    }

    private fun displayTriggerSnackbar(mark: MapMark) =
        displayTriggerSnackbar(mark.id)

    private fun displayTriggerSnackbar(text: String) = Snackbar.make(
        requireView(),
        "Triggered mark with id: $text",
        Snackbar.LENGTH_LONG
    ).also {
        it.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                snackbar = null
            }
        })
        it.show()
    }

    private fun RectF.toSizeI() = Size(width().toInt(), height().toInt())
}