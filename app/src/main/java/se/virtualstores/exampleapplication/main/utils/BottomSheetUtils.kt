package se.virtualstores.exampleapplication.main.utils

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

fun BottomSheetBehavior<out View>.setStateCallback(callback: (View, Int) -> Unit) {
    addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(p0: View, p1: Float) {}
        override fun onStateChanged(view: View, state: Int) {
            callback(view, state)
        }
    })
}

fun BottomSheetBehavior<out View>.setSlideCallback(callback: (View, Float) -> Unit) {
    addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(view: View, offset: Float) {
            callback(view, offset)
        }

        override fun onStateChanged(view: View, state: Int) {}
    })
}