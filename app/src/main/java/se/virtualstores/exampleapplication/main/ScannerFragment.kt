package se.virtualstores.exampleapplication.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_scanner.*
import kotlinx.android.synthetic.main.fragment_scanner.view.*
import se.virtualstores.exampleapplication.R
import se.virtualstores.exampleapplication.main.utils.setStateCallback
import se.virtualstores.tt2.androidsdk.scanner.HandlerScannerFragment
import se.virtualstores.tt2.androidsdk.scanner.view.ScannerView

class ScannerFragment : HandlerScannerFragment() {
    override val scannerView: ScannerView
        get() = view?.findViewById(R.id.scannerView)
            ?: throw IllegalStateException("No MapView found")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.abortButton.setOnClickListener {
            cancel()
        }
        setupBottomSheet()
        background.setOnClickListener { cancel() }
    }

    override fun validateResult(result: String?): Boolean {
        return true
    }

    private fun setupBottomSheet() {
        val sheetBehavior = BottomSheetBehavior.from(requireView().cameraSheet)
        sheetBehavior.setStateCallback { _, state ->
            if (state == BottomSheetBehavior.STATE_HIDDEN) cancel()
        }
    }

    override fun onPreRequestCameraPermissions(callback: (Boolean) -> Unit) {
        callback(true)
    }
}