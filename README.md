# TT2SDK

This Readme will help you get started with the Example Project for your Virtual Store

### Who is this repository for? ###
Customers at Virtual Stores which is planning on implementing a Virtual Store in their own app

### What is this repository for? ###
A way to get started working with Virtual Stores TT2SDK as fast as possible


### How do I get set up? ###
For initial setup you need to contact Virtual Stores
http://www.virtualstores.se/

* Dependencies
	To build the project you need Gradle 6.3.2

### Who do I talk to? ###
http://www.virtualstores.se/